package com.example.demo.service;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class ToDoServiceTest {

    private ToDoRepository toDoRepository;

    private ToDoService toDoService;

    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        this.toDoService = new ToDoService(toDoRepository);
    }

    @Test
    void whenGetAllWithCompletedNull_thenRepositoryFindAllCalled() {
        toDoService.getAll(null);

        verify(toDoRepository, times(1)).findAll();
    }

    @Test
    void whenGetAllWithCompletedTrue_thenRepositoryFindByCompletedAtIsNotNullCalled() {
        toDoService.getAll(true);

        verify(toDoRepository, times(1)).findByCompletedAtIsNotNull();
    }

    @Test
    void whenGetAllWithCompletedFalse_thenRepositoryFindByCompletedAtIsNullCalled() {
        toDoService.getAll(false);

        verify(toDoRepository, times(1)).findByCompletedAtIsNull();
    }

    @Test
    void whenCompleteIdNotFound_thenThrowNotFoundException() {
        assertThrows(ToDoNotFoundException.class, () -> toDoService.completeToDo(1l));
    }

    @Test
    void whenUpsertIdNotFound_thenThrowNotFoundException() {
        var toDoDto = new ToDoSaveRequest();
        toDoDto.id = 1l;
        assertThrows(ToDoNotFoundException.class, () -> toDoService.upsert(toDoDto));
    }

    @Test
    void whenCompleteAlreadyDone_thenCompletedAtNotChanged() throws ToDoNotFoundException {
        var completedAt = ZonedDateTime.now(ZoneOffset.UTC);
        var todo = new ToDoEntity(0l, "Test 1", completedAt);
        when(toDoRepository.findById(todo.getId())).thenReturn(Optional.of(todo));
        ArgumentMatcher<ToDoEntity> todoMatcher = (ToDoEntity saveTodo) ->
                todo.getId().equals(saveTodo.getId()) && todo.getText().equals(saveTodo.getText());
        when(toDoRepository.save(argThat(todoMatcher))).thenAnswer(i -> {
            ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
            Long id = arg.getId();
            if (id.equals(todo.getId())) {
                return todo;
            } else {
                return new ToDoEntity();
            }
        });

        toDoService.completeToDo(todo.getId());

        assertEquals(completedAt, todo.getCompletedAt());
    }
}
