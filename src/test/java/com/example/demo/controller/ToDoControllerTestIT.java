package com.example.demo.controller;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
@Transactional
class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void whenGetIdNotFound_thenNotFoundException() throws Exception {
        Long id = 33L;

        mockMvc.perform(get("/todos/{id}", id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ToDoNotFoundException))
                .andExpect(result -> assertEquals(String.format("Can not find todo with id %d", id),
                        result.getResolvedException().getMessage()));
    }

    @Test
    void whenGetAllWithCompletedNull_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].id", allOf(notNullValue(Long.class))))
                .andExpect(jsonPath("$[*].text", containsInAnyOrder(
                        "Wash the dishes",
                        "Learn to test Java app")))
                .andExpect(jsonPath("$[*].completedAt", hasItem(notNullValue(Date.class))));
    }

    @Test
    void whenGetAllWithCompletedTrue_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(get("/todos").param("completed", Boolean.TRUE.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].text").value("Learn to test Java app"))
                .andExpect(jsonPath("$[0].completedAt").exists());
    }

    @Test
    void whenGetAllWithCompletedFalse_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(get("/todos").param("completed", Boolean.FALSE.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].text").value("Wash the dishes"))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Test
    void whenSaveNew_thenReturnValidResponse() throws Exception {
        ToDoSaveRequest toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.text = "New todo item";
        String requestJson = convertToJson(toDoSaveRequest);

        mockMvc.perform(post("/todos").contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.text").value(toDoSaveRequest.text))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenSaveExisting_thenReturnValidResponse() throws Exception {
        ToDoSaveRequest toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.id = 1L;
        toDoSaveRequest.text = "New todo item";
        String requestJson = convertToJson(toDoSaveRequest);

        mockMvc.perform(post("/todos").contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(toDoSaveRequest.id))
                .andExpect(jsonPath("$.text").value(toDoSaveRequest.text))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenComplete_thenReturnValidResponse() throws Exception {
        Long toDoId = 1L;
        var startTime = ZonedDateTime.now(ZoneOffset.UTC);

        mockMvc.perform(put("/todos/{id}/complete", toDoId).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(toDoId))
                .andExpect(jsonPath("$.text").value("Wash the dishes"))
                .andExpect(jsonPath("$.completedAt").exists())
                .andDo(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    var response = objectMapper.readValue(json, ToDoResponse.class);
                    assertTrue(response.completedAt.isAfter(startTime));
                });
    }

    private String convertToJson(Object object) throws JsonProcessingException {
        ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(object);
        return json;
    }
}

